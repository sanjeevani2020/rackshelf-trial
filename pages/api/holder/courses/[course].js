// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
const yaml = require('js-yaml');
const fs = require('fs');

export default (req, res) => {
    const {
        query: { course },
    } = req

    try {
        const doc = yaml.load(fs.readFileSync(`https://rackshelf-trial.vercel.app/holder/courses/${course}.yml`, 'utf8'));
        console.log(doc);
        res.statusCode = 200
        res.json({
            success: true,
            content: doc
        })
    } catch (e) {
        console.log(e);
        res.statusCode = 400
        res.json({
            success: false,
            message: course
        })
    }
}